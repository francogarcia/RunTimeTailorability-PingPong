extends Node


# TODO Background loading
# <http://docs.godotengine.org/en/latest/learning/features/misc/background_loading.html>.

const k_Splash = 0
const k_PingPong = 1

const k_SplashString = "Splash"
const k_PingPongString = "PingPong"

const k_SplashScene = preload("res://scenes/splash/splash.tscn")
const k_PingPongScene = preload("res://scenes/meta-game/meta_game.tscn")

const k_AbstractField = "Field"
# const k_ClassAbstractField = preload("res://scenes/meta-game/entities/abstract_field.gd")
const k_ClassAbstractFieldSceneResource = "res://scenes/meta-game/entities/abstract_field.tscn"
const k_ClassAbstractFieldScene = preload("res://scenes/meta-game/entities/abstract_field.tscn")

const k_AbstractPlayer1 = "Player1"
const k_ClassAbstractPlayer1 = preload("res://scenes/meta-game/entities/abstract_player1.gd")
const k_ClassAbstractPlayer1SceneResource = "res://scenes/meta-game/entities/abstract_player1.tscn"
const k_ClassAbstractPlayer1Scene = preload("res://scenes/meta-game/entities/abstract_player1.tscn")

const k_AbstractPlayer2 = "Player2"
const k_ClassAbstractPlayer2 = preload("res://scenes/meta-game/entities/abstract_player2.gd")
const k_ClassAbstractPlayer2SceneResource = "res://scenes/meta-game/entities/abstract_player2.tscn"
const k_ClassAbstractPlayer2Scene = preload("res://scenes/meta-game/entities/abstract_player2.tscn")

const k_AbstractBall = "Ball"
const k_ClassAbstractBall = preload("res://scenes/meta-game/entities/abstract_ball.gd")
const k_ClassAbstractBallSceneResource = "res://scenes/meta-game/entities/abstract_ball.tscn"
const k_ClassAbstractBallScene = preload("res://scenes/meta-game/entities/abstract_ball.tscn")



# Subsystems

# const k_GameInput = preload("res://scenes/base_game/base_game_input.tscn")
# const k_MenuInput = preload("res://scenes/base_game/base_menu_input.tscn")

# const k_GameOutput = preload("res://scenes/base_game/base_game_output.tscn")
# const k_MenuOutput = preload("res://scenes/base_game/base_menu_output.tscn")



var m_CurrentSceneKey = null
var m_NextSceneKey = null
var m_CurrentSceneName = null

var m_Overlay = null

var m_SharedData = null


func _ready():
    set_fixed_process(true)

    m_CurrentSceneKey = null
    m_NextSceneKey = null
    m_CurrentSceneName = null

    m_SharedData = {
        "PlayerAbilities": {

        },
        "Scenes": {
            # TODO Choose which data to provide as scene data or general data.
            # TODO Use a function to retrieve constant values.
            "Splash": {
                shared = ""
            },
            "PingPong": {
                data = ""
            },
        }
    }

    m_Overlay = null

    register_input_actions()


func _fixed_process(delta):
    if (m_NextSceneKey != null):
        m_CurrentSceneKey = m_NextSceneKey
        tailor_game_scene(m_CurrentSceneKey, get_current_scene())
        m_NextSceneKey = null


func register_input_actions():
    # Menus
    InputMap.add_action("confirm")
    InputMap.add_action("next_option")
    InputMap.add_action("previous_option")
    InputMap.add_action("set_option")

    # Game
    InputMap.add_action("player1_move_left")
    InputMap.add_action("player1_move_right")
    InputMap.add_action("player2_move_left")
    InputMap.add_action("player2_move_right")


func instance_abstract_scene(sceneKey):
    # TODO Perhaps we should remove these hard coded strings.
    var abstractScene = null
    if (sceneKey == k_Splash):
        abstractScene = k_SplashScene
        abstractScene.set_name(k_SplashString)
    elif (sceneKey == k_PingPong):
        abstractScene = k_PingPongScene
        abstractScene.set_name(k_PingPongString)
    # elif (sceneKey == k_):
    #     abstractScene = k_Scene
    #     abstractScene.set_name(k_String)
    else:
        assert(!"Invalid scene!")

    return abstractScene


func tailor_game_scene(sceneKey, abstractScene):
    # NOTE Currently adding an UI canvas layer to all scenes. It could be
    # instanced per scene instead (in instance_abstract_scene(), for instance).

    # Attach an user-interface overlay to the tailored scene.
    m_Overlay = load("res://scenes/user_interfaces/user_interface_overlay.tscn").instance()
    m_Overlay.set_layer(1) # It should be above the game's scene.
    abstractScene.add_child(m_Overlay)

    # TODO We could auto-load the overlay scene to reduce the loading overhead.
    # NOTE Provided we ensure the correct execution order, we could remove the
    # conditions at this point. The entire tailoring process is performed in the
    # functions.
    # Wrong order:
    # 1. overlay ready
    # 2. input ready
    # 3. output ready
    # 4. overlay destructed
    # 5. meta game ready
    # Correct order:
    # 1. meta game ready
    # 2. overlay ready
    # 3. input ready
    # 4. output ready
    # Implementation here:
    # m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
    # Tailor the game's IO according to the profile.
    # g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    if (sceneKey == k_Splash):
        m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
        # Tailor the IO according to the profile.
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), k_SplashString)
    elif (sceneKey == k_PingPong):
        m_CurrentSceneName = get_scene_string_from_scene_key(sceneKey)
        # Tailor the IO according to the profile.
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), k_PingPongString)
    else:
        assert(!"Invalid scene!")


func untailor_game_scene(sceneKey, abstractScene):
    # TODO Switch to the line below (same above for tailor_game_scene()):
    # g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), m_CurrentSceneName)
    if (sceneKey == k_Splash):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), k_SplashString)
    elif (sceneKey == k_PingPong):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), k_PingPongString)
    else:
        assert(!"Invalid scene!")


func change_scene_to(sceneKey):
    if (m_CurrentSceneKey != null):
        # Remove any tailoring performed to the scene.
        untailor_game_scene(m_CurrentSceneKey, get_current_scene())

    # Prepare next scene.
    m_NextSceneKey = sceneKey

    var abstractScene = instance_abstract_scene(sceneKey)
    get_tree().change_scene_to(abstractScene)

    # NOTE Another option is to load the scene here, and manually add it to the tree:
    # tailor_game_scene(sceneKey, abstractScene)
    # m_CurrentScene.queue_free()
    # get_tree().get_root().add_child(m_CurrentScene)


func attach_scene_to(scene, destinationScene):
    assert(scene != null)
    assert(destinationScene != null)

    var newScene = scene.instance()
    destinationScene.add_child(newScene)


func detach_scene_from(sceneResource, sourceScene):
    assert(sceneResource != null)
    assert(sourceScene != null)

    for child in sourceScene.get_children():
        if (child.get_filename() == sceneResource):
            return child

    return null


func get_current_scene_key():
    return m_CurrentSceneKey


func get_current_scene():
    return get_tree().get_current_scene()


func get_current_scene_name():
    return m_CurrentSceneName


func get_scene_string_from_scene_key(sceneKey):
    if (sceneKey == k_Splash):
        return k_SplashString
    elif (sceneKey == k_PingPong):
        return k_PingPongString
    else:
        assert(!"Invalid option!")


func set_shared_scene_data(sceneKey, data):
    var sceneKeyString = get_scene_string_from_scene_key(sceneKey)
    m_SharedData["Scenes"][sceneKeyString] = data


func get_shared_scene_data(sceneKey):
    var sceneKeyString = get_scene_string_from_scene_key(sceneKey)
    return m_SharedData["Scenes"][sceneKeyString]


func get_user_interface_overlay():
    assert(m_Overlay)

    return m_Overlay


func get_abstract_entity_scene_path(sceneKey, entityKey):
    if (sceneKey == k_Splash):
        assert(!"Not implemented yet!")
    elif (sceneKey == k_PingPong):
        if (entityKey == k_AbstractBall):
            return k_ClassAbstractBallSceneResource
        elif (entityKey == k_AbstractPlayer1):
            return k_ClassAbstractPlayer1SceneResource
        elif (entityKey == k_AbstractPlayer2):
            return k_ClassAbstractPlayer2SceneResource
        elif (entityKey == k_AbstractField):
            return k_ClassAbstractFieldSceneResource
    assert(!"Invalid scene!")


func get_abstract_entity_key(sceneKey, entityScenePath):
    if (sceneKey == k_Splash):
        assert(!"Not implemented yet!")
    elif (sceneKey == k_PingPong):
        if (entityScenePath == k_ClassAbstractBallSceneResource):
            return k_AbstractBall
        elif (entityScenePath == k_ClassAbstractPlayer1SceneResource):
            return k_AbstractPlayer1
        elif (entityScenePath == k_ClassAbstractPlayer2SceneResource):
            return k_AbstractPlayer2
        elif (entityScenePath == k_ClassAbstractBallSceneResource):
            return k_AbstractBall
    assert(!"Invalid scene!")


func get_abstract_entity_scene(sceneKey, entityKey):
    if (sceneKey == k_Splash):
        assert(!"Not implemented yet!")
    elif (sceneKey == k_PingPong):
        if (entityKey == k_AbstractBall):
            return k_ClassAbstractBallScene
        elif (entityKey == k_AbstractPlayer1):
            return k_ClassAbstractPlayer1Scene
        elif (entityKey == k_AbstractPlayer2):
            return k_ClassAbstractPlayer2Scene
        elif (entityKey == k_AbstractBall):
            return k_ClassAbstractBall
    assert(!"Invalid scene!")


# NOTE This can be improved to work as a factory pattern.
func instance_abstract_entity_scene(sceneKey, entityKey):
    # TODO Swap here if Godot allows defining constants from other constants in
    # the future. var resource = get_abstract_entity_scene_path(sceneKey, entityKey)
    var resource = get_abstract_entity_scene(sceneKey, entityKey)

    # TODO If expanding for a factory, trigger events for creation and
    # destruction of entities.

    return resource.instance()
