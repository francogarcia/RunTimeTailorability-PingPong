extends Node


# NOTE If untailoring and tailoring the game in the same frame, Godot will
# change the name of the new node to avoid duplicating the old one. This might
# be a problem if a IO subystem tries to find/use an IO component by name.
# To avoid this, this implementation rename a node to
# "[removed]" before freeing it.
# Possible alternatives:
# - Call free() instead of queue_free();
# - Make subsystems find node by pattern instead of name;
# - Do not untailor and tailor the meta-game in the same frame;
# - Untailor at a frame, tailor at next frame.


var m_InteractionProfile = null

var m_bTailored = null


func _ready():
    # Run-time tailoring.

    # Initial interaction profile.
    m_InteractionProfile = load_interaction_profile("res://profiles/usual_game.json")
    # tailor_to_profile(m_InteractionProfile)

    m_bTailored = false


func load_interaction_profile(interactionProfileFilename):
    var file = File.new()
    var error = file.open(interactionProfileFilename, File.READ)
    assert(error == OK)

    var fileContent = file.get_as_text()
    var interactionProfile = {}
    interactionProfile.parse_json(fileContent)

    file.close()

    return interactionProfile


func load_and_set_interaction_profile(interactionProfileFilename):
    m_InteractionProfile = load_interaction_profile(interactionProfileFilename)

    return m_InteractionProfile


# NOTE Also update untailor_to_profile() when modifying this.
func tailor_to_profile(interactionProfile, sceneName):
    # NOTE Currently we support only one profile at time.
    # Multiple profiles at once might make sense for more granular game composition.
    if (m_bTailored):
        untailor_to_profile(interactionProfile, sceneName)

    # Input mapping.
    tailor_input_mapping(interactionProfile, sceneName)

    # Register event handlers.
    tailor_event_handlers(interactionProfile, sceneName)

    # Tailor existing entities.
    tailor_entities(interactionProfile, sceneName)

    # Add input and output subsystems.
    tailor_input_subsystems(interactionProfile, sceneName)
    tailor_output_subsystems(interactionProfile, sceneName)

    m_bTailored = true


# NOTE Also update tailor_to_profile() when modifying this.
func untailor_to_profile(interactionProfile, sceneName):
    if (not m_bTailored):
        return

    # Remove input mapping.
    untailor_input_mapping(interactionProfile, sceneName)

    # Remove event handlers.
    untailor_event_handlers(interactionProfile, sceneName)

    # Untailor existing entities.
    untailor_entities(interactionProfile, sceneName)

    # Remove input and output subsystems.
    untailor_input_subsystems(interactionProfile, sceneName)
    untailor_output_subsystems(interactionProfile, sceneName)

    m_bTailored = false


func tailor_input_mapping(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var inputMapping = sceneInteractionProfile["InputMapping"]
    for inputAction in inputMapping:
        var actionMapping = inputMapping[inputAction]
        var inputDevice = actionMapping[0]
        var inputDeviceBinding = actionMapping[1]
        var inputEvent = make_input_event(inputDevice, inputDeviceBinding)
        if (inputEvent != null):
            InputMap.action_add_event(inputAction, inputEvent)
        else:
            # Let the controller do work with input automation.
            var inputEvent = actionMapping[1]
            var inputAutomationHandler = actionMapping[2]
            # Register an event handler to perform the input.
            g_MetaGameEvents.connect(inputEvent, g_EventHandlers, inputAutomationHandler)


func tailor_event_handlers(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var eventSpecialization = sceneInteractionProfile["EventSpecialization"]
    for eventName in eventSpecialization:
        # print(eventName)
        var eventHandlers = eventSpecialization[eventName]
        for eventHandler in eventHandlers:
            # print("  -> ", eventHandler)
            g_MetaGameEvents.connect(eventName, g_EventHandlers, eventHandler)


func tailor_entities(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var entityComponentSpecialization = sceneInteractionProfile["ComponentSpecialization"]
    var currentSceneKey = g_SceneManager.get_current_scene_key()
    var currentScene = g_SceneManager.get_current_scene()
    for entityName in entityComponentSpecialization:
        var entityData = entityComponentSpecialization[entityName]
        var abstractEntityResource = g_SceneManager.get_abstract_entity_scene_path(currentSceneKey, entityName)
        for componentSpecializationResource in entityData:
            for node in currentScene.get_children():
                # printt(node.get_name(), node.get_filename(), node.get_path())
                tailor_suitable_entities(node, abstractEntityResource, componentSpecializationResource)


func tailor_input_subsystems(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var inputSubystemSpecialization = sceneInteractionProfile["InputSubsystemSpecialization"]
    var currentSceneKey = g_SceneManager.get_current_scene_key()
    var currentScene = g_SceneManager.get_current_scene()

    for subsystemName in inputSubystemSpecialization:
        var scene = load(inputSubystemSpecialization[subsystemName])
        scene.set_name(subsystemName)
        g_SceneManager.attach_scene_to(scene, currentScene)


func tailor_output_subsystems(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var outputSubystemSpecialization = sceneInteractionProfile["OutputSubsystemSpecialization"]
    var currentScene = g_SceneManager.get_current_scene()

    for subsystemName in outputSubystemSpecialization:
        var scene = load(outputSubystemSpecialization[subsystemName])
        scene.set_name(subsystemName)
        g_SceneManager.attach_scene_to(scene, currentScene)


func untailor_input_mapping(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var inputMapping = sceneInteractionProfile["InputMapping"]
    for inputAction in inputMapping:
        var actionMapping = inputMapping[inputAction]
        var inputDevice = actionMapping[0]
        var inputDeviceBinding = actionMapping[1]
        var inputEvent = make_input_event(inputDevice, inputDeviceBinding)
        if (inputEvent != null):
            InputMap.action_erase_event(inputAction, inputEvent)
        else:
            var inputEvent = actionMapping[1]
            var inputAutomationHandler = actionMapping[2]
            g_MetaGameEvents.disconnect(inputEvent, g_EventHandlers, inputAutomationHandler)


func untailor_event_handlers(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var eventSpecialization = sceneInteractionProfile["EventSpecialization"]
    for eventName in eventSpecialization:
        # print(eventName)
        var eventHandlers = eventSpecialization[eventName]
        for eventHandler in eventHandlers:
            # print("  -> ", eventHandler)
            g_MetaGameEvents.disconnect(eventName, g_EventHandlers, eventHandler)


func untailor_entities(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var entityComponentSpecialization = sceneInteractionProfile["ComponentSpecialization"]
    var currentSceneKey = g_SceneManager.get_current_scene_key()
    var currentScene = g_SceneManager.get_current_scene()
    for entityName in entityComponentSpecialization:
        var entityData = entityComponentSpecialization[entityName]
        var abstractEntityResource = g_SceneManager.get_abstract_entity_scene_path(currentSceneKey, entityName)
        for componentSpecializationResource in entityData:
            for node in currentScene.get_children():
                # printt(node.get_name(), node.get_filename(), node.get_path())
                untailor_suitable_entities(node, abstractEntityResource, componentSpecializationResource)


func untailor_input_subsystems(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var inputSubystemSpecialization = sceneInteractionProfile["InputSubsystemSpecialization"]
    var currentSceneKey = g_SceneManager.get_current_scene_key()
    var currentScene = g_SceneManager.get_current_scene()

    for subsystemName in inputSubystemSpecialization:
        var scene = g_SceneManager.detach_scene_from(inputSubystemSpecialization[subsystemName], currentScene)
        if (scene != null):
            scene.queue_free()


func untailor_output_subsystems(interactionProfile, sceneName):
    var sceneInteractionProfile = interactionProfile["InteractionProfile"]["Scenes"][sceneName]
    var outputSubystemSpecialization = sceneInteractionProfile["OutputSubsystemSpecialization"]
    var currentScene = g_SceneManager.get_current_scene()

    for subsystemName in outputSubystemSpecialization:
        var scene = g_SceneManager.detach_scene_from(outputSubystemSpecialization[subsystemName], currentScene)
        if (scene != null):
            scene.queue_free()


func tailor_suitable_entities(node, abstractEntityResource, componentSpecializationResource):
    if (node.get_filename() == abstractEntityResource):
        # TODO This could be an argument to speed-up the function.
        var concreteEntity = load(componentSpecializationResource).instance()
        node.add_child(concreteEntity)

    for child in node.get_children():
        tailor_suitable_entities(child, abstractEntityResource, componentSpecializationResource)


func untailor_suitable_entities(node, abstractEntityResource, componentSpecializationResource):
    for child in node.get_children():
        untailor_suitable_entities(child, abstractEntityResource, componentSpecializationResource)

    # TODO Perhaps we could free this first and skip the next step, as all
    # children should be concrete as well. With this, the third parameter could
    # be removed as well -- we would use only the second.
    # if (node.get_filename() in abstractEntityResource):
    #     node.queue_free()
    if (node.get_filename() in componentSpecializationResource):
        node.set_name("[removed]")
        node.queue_free()


func call_handler_on_entity(eventSpecializationProfile, eventName, entity):
    assert(entity != null)
    if (eventSpecializationProfile.has(eventName)):
        var eventHandlers = eventSpecializationProfile[eventName]
        for eventHandler in eventHandlers:
            g_EventHandlers.call(eventHandler, entity)


func call_handler_on_entities(eventSpecializationProfile, eventName, entities):
    assert(entities != null)
    var entitiesCount = entities.get_child_count()
    for entityIndex in range(0, entitiesCount):
        call_handler_on_entity(eventSpecializationProfile, eventName, entities.get_child(entityIndex))


func make_input_event(inputDevice, actionMapping):
    # if (inputDevice.to_lower() == "automation"):
    if (inputDevice == "Automation"):
        return null
    else:
        return string_to_input_action_mapping(inputDevice, actionMapping)


# NOTE When modifying this, also update string_to_input_action_mapping().
func input_action_mapping_to_string(inputEventMapping):
    var device = null
    var button = null
    if (inputEventMapping.type == InputEvent.KEY):
        # parse_value(): "key"
        device = "Keyboard"
        # Keycodes: https://github.com/godotengine/godot/blob/master/core/os/keyboard.cpp
        button = OS.get_scancode_string(inputEventMapping.scancode)
    elif (inputEventMapping.type == InputEvent.JOYSTICK_BUTTON):
        # parse_value(): "jbutton"
        device = "Joystick Button"
        button = Input.get_joy_button_string(inputEventMapping.button_index)
    elif (inputEventMapping.type == InputEvent.JOYSTICK_MOTION):
        # parse_value(): "jaxis"
        device = "Joystick Axis"
        button = Input.get_joy_axis_string(inputEventMapping.axis)
    elif (inputEventMapping.type == InputEvent.MOUSE_BUTTON):
        # parse_value(): "mbutton"
        device = "Mouse Button"
        button = null
        var buttonIndex = inputEventMapping.button_index
        if (buttonIndex == BUTTON_LEFT): # == 1
            button = "Left"
        elif (buttonIndex == BUTTON_RIGHT): # == 2
            button = "Right"
        elif (buttonIndex == BUTTON_MIDDLE): # == 3
            button = "Middle"
        elif (buttonIndex == BUTTON_WHEEL_UP): # == 4
            button = "Wheel Up"
        elif (buttonIndex == BUTTON_WHEEL_DOWN): # == 5
            button = "Wheel Down"
        # elif (buttonIndex == BUTTON_WHEEL_LEFT): # == 6 (not shown in Project Settings).
        #     button = "wheel left"
        # elif (buttonIndex == BUTTON_WHEEL_RIGHT): # == 7 (not shown in Project Settings).
        #     button = "wheel right"
        elif (buttonIndex == 6): # BUTTON_6):
            button = "Button 6"
        elif (buttonIndex == 7): # BUTTON_7):
            button = "Button 7"
        elif (buttonIndex == 8): # BUTTON_8):
            button = "Button 8"
        elif (buttonIndex == 9): # BUTTON_9):
            button = "Button 9"
        else:
            assert("Mouse button not found!")
    else:
        assert(!"Input device not supported!")

    return [device, button]


# NOTE When modifying this, also update input_action_mapping_to_string().
func string_to_input_action_mapping(device, button):
    # NOTE Godot functions are case sensitive. If this changes, update here,
    # input_action_mapping_to_string(), and make_input_event() to use lower case
    # versions of the strings instead.
    # device = device.to_lower()
    # button = button.to_lower()
    var inputEvent = InputEvent()
    if (device == "Keyboard"):
        inputEvent.type = InputEvent.KEY
        # Keycodes: https://github.com/godotengine/godot/blob/master/core/os/keyboard.cpp
        inputEvent.scancode = OS.find_scancode_from_string(button)
    elif (device == "Joystick Button"):
        inputEvent.type = InputEvent.JOYSTICK_BUTTON
        inputEvent.button_index = Input.get_joy_button_index_from_string(button)
    elif (device == "Joystick Axis"):
        inputEvent.type = InputEvent.JOYSTICK_MOTION
        inputEvent.axis = Input.get_joy_axis_index_from_string(button)
    elif (device == "Mouse Button"):
        inputEvent.type = InputEvent.MOUSE_BUTTON
        if (button == "Left"):
            inputEvent.button_index = BUTTON_LEFT # == 1
        elif (button == "Right"):
            inputEvent.button_index = BUTTON_RIGHT # == 2
        elif (button == "Middle"):
            inputEvent.button_index = BUTTON_MIDDLE # == 3
        elif (button == "Wheel Up"):
            inputEvent.button_index = BUTTON_WHEEL_UP # == 4
        elif (button == "Wheel Down"):
            inputEvent.button_index = BUTTON_WHEEL_DOWN # == 5
        # elif (button == "Wheel Left"):
        #     inputEvent.button_index = BUTTON_WHEEL_LEFT # == 6 (not shown in Project Settings).
        # elif (button == "Wheel Right"):
        #     inputEvent.button_index = BUTTON_WHEEL_RIGHT # == 7 (not shown in Project Settings).
        elif (button == "Button 6"):
            inputEvent.button_index = 6 # BUTTON_6
        elif (button == "Button 7"):
            inputEvent.button_index = 7 # BUTTON_7
        elif (button == "Button 8"):
            inputEvent.button_index = 8 # BUTTON_8
        elif (button == "Button 9"):
            inputEvent.button_index = 9 # BUTTON_9
        else:
            assert("Mouse button not found!")
    else:
        assert(!"Input device not supported!")
        inputEvent = null

    return inputEvent


func register_keyboard_key(action, key):
    var inputEvent = string_to_input_action_mapping("Keyboard", key)
    InputMap.action_add_event(action, inputEvent)


func get_interaction_profile():
    assert(m_InteractionProfile != null)

    return m_InteractionProfile


func is_tailored():
    return m_bTailored
