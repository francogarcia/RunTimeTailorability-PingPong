extends Control


func _ready():
    set_fixed_process(true)


func _fixed_process(delta):
    update()


func _draw():
    draw_entity("Player1")
    draw_entity("Player2")
    draw_entity("Ball")
    # draw_entity("Field")


func draw_entity(name):
    var metaGame = get_node("/root/MetaGame")
    var entity = metaGame.get_node(name)

    var position = entity.get_node("Transformable").get_pos()

    var width = entity.get_node("Collidable").m_Width
    var height = entity.get_node("Collidable").m_Height
    var halfWidth = width / 2.0
    var halfHeight = height / 2.0
    var topLeft = position - Vector2(halfWidth, halfHeight)
    draw_line(topLeft, Vector2(topLeft.x + width, topLeft.y), Color(1.0, 1.0, 1.0))
    draw_line(topLeft, Vector2(topLeft.x, topLeft.y + height), Color(1.0, 1.0, 1.0))
    draw_line(Vector2(topLeft.x + width, topLeft.y), Vector2(topLeft.x + width, topLeft.y + height), Color(1.0, 1.0, 1.0))
    draw_line(Vector2(topLeft.x, topLeft.y + height), Vector2(topLeft.x + width, topLeft.y + height), Color(1.0, 1.0, 1.0))
