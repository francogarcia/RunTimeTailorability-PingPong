extends Node


# TODO Time elapsed since this interface was created.
var m_TimeElapsed = null


func _ready():
    set_process(true)

    on_event_goal_score_text_output()

    # Register event handlers.
    g_MetaGameEvents.connect("EventGoal", self, "on_event_goal_score_text_output")


func _process(delta):
    m_TimeElapsed += delta
    update_score()


func update_score():
    var metaGame = get_node("/root/MetaGame")
    get_node("RoundTime").set_text("Round time: %.02fs" % m_TimeElapsed)


func on_event_goal_score_text_output():
    var metaGame = get_node("/root/MetaGame")
    get_node("Player1Score").set_text("Player 1: %d" % metaGame.m_Player1Score)
    get_node("Player2Score").set_text("Player 2: %d" % metaGame.m_Player2Score)

    m_TimeElapsed = 0.0
