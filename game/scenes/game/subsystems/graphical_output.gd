extends Node


func _ready():
    set_process(true)


func _process(delta):
    var metaGame = get_node("/root/MetaGame")
    var field = metaGame.get_node("Field")
    var fieldPosition = field.get_node("Transformable").get_pos()
    var fieldSize = Vector2(field.get_node("Collidable").m_Width, field.get_node("Collidable").m_Height)
    if (field.has_node("Drawable")):
        var fieldSprite = field.get_node("Drawable")
        fieldSprite.set_pos(fieldPosition)

    var ball = metaGame.get_node("Ball")
    var ballPosition = ball.get_node("Transformable").get_pos()
    var ballSize = Vector2(ball.get_node("Collidable").m_Width, ball.get_node("Collidable").m_Height)
    if (ball.has_node("Drawable")):
        var ballSprite = ball.get_node("Drawable")
        ballSprite.set_pos(ballPosition)

    var player1 = metaGame.get_node("Player1")
    var player1Position = player1.get_node("Transformable").get_pos()
    var player1Size = Vector2(player1.get_node("Collidable").m_Width, player1.get_node("Collidable").m_Height)
    if (player1.has_node("Drawable")):
        var player1Sprite = player1.get_node("Drawable")
        player1Sprite.set_pos(player1Position)

    var player2 = metaGame.get_node("Player2")
    var player2Position = player2.get_node("Transformable").get_pos()
    var player2Size = Vector2(player2.get_node("Collidable").m_Width, player2.get_node("Collidable").m_Height)
    if (player2.has_node("Drawable")):
        var player2Sprite = player2.get_node("Drawable")
        player2Sprite.set_pos(player2Position)
