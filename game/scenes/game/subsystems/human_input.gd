extends Node


func _ready():
    set_process(true)


func _process(delta):
    human_input()


func human_input():
    var metaGame = get_node("/root/MetaGame")
    if (metaGame.get_node("Player1").has_node("HumanInput")):
        if (Input.is_action_pressed("player1_move_left")):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player1, metaGame.k_PlayerMovementLeft)
        if (Input.is_action_pressed("player1_move_right")):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player1, metaGame.k_PlayerMovementRight)

    if (metaGame.get_node("Player2").has_node("HumanInput")):
        if (Input.is_action_pressed("player2_move_left")):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player2, metaGame.k_PlayerMovementLeft)
        if (Input.is_action_pressed("player2_move_right")):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player2, metaGame.k_PlayerMovementRight)
