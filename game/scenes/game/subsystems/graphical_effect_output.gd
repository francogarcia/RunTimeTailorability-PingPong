extends Node


const k_GraphicalEffectSceneCollision = preload("res://scenes/game/entities/collision_cloud.tscn")
const k_GraphicalEffectSceneGoal = preload("res://scenes/game/entities/star.tscn")


func _ready():
    # Register event handlers.
    #g_MetaGameEvents.connect("EventBallReleased", self, "on_event_ball_released_graphical_effect")
    g_MetaGameEvents.connect("EventGoal", self, "on_event_goal_graphical_effect")
    g_MetaGameEvents.connect("EventBallHitWall", self, "on_event_ball_hit_wall_graphical_effect")
    g_MetaGameEvents.connect("EventBallHitPaddle", self, "on_event_ball_hit_paddle_graphical_effect")
    # g_MetaGameEvents.connect("EventBallMoved", self, "on_event_ball_moved_graphical_effect")
    # g_MetaGameEvents.connect("EventPaddleMoved", self, "on_event_paddle_moved_graphical_effect")


func on_event_ball_hit_wall_graphical_effect(wallIndex, position):
    add_graphical_effect(position, Color(1.0, 1.0, 1.0, 0.5))


func on_event_ball_hit_paddle_graphical_effect(playerIndex, position):
    var metaGame = get_node("/root/MetaGame")
    var color = Color(0.0, 0.0, 0.0, 0.5)
    if (playerIndex == metaGame.k_Player1):
        color.b = 1.0
    elif (playerIndex == metaGame.k_Player2):
        color.r = 1.0
    add_graphical_effect(position, color)


func on_event_goal_graphical_effect(playerIndex):
    var metaGame = get_node("/root/MetaGame")

    var position
    if (playerIndex == metaGame.k_Player1):
        position = metaGame.get_node("Player1/Transformable").get_pos()
    else:
        position = metaGame.get_node("Player2/Transformable").get_pos()

    var graphicalEffect = k_GraphicalEffectSceneGoal.instance()
    graphicalEffect.set_pos(position)
    add_child(graphicalEffect)

    var timer = Timer.new()
    timer.set_wait_time(0.5)
    timer.start()
    timer.connect("timeout", self, "on_special_effect_time_out", [graphicalEffect])
    graphicalEffect.add_child(timer)


func add_graphical_effect(position, color):
    var graphicalEffect = k_GraphicalEffectSceneCollision.instance()
    graphicalEffect.set_pos(position)
    graphicalEffect.set_modulate(color)
    add_child(graphicalEffect)

    var timer = Timer.new()
    timer.set_wait_time(0.5)
    timer.start()
    timer.connect("timeout", self, "on_special_effect_time_out", [graphicalEffect])
    graphicalEffect.add_child(timer)

func on_special_effect_time_out(effectNode):
    effectNode.queue_free()
