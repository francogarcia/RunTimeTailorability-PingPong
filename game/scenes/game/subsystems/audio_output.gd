extends Node


# TODO FIXME Godot does not clear nodes at once; it pre-deletes them.

# If we untailor and tailor the same scene again (or if we switch profiles
# directly to other profiles using a subsystem with the same events), the signal
# will still be registered to an event before being unregistering to it. As a
# result, the sound effect will be unregistered incorrectly (wrong order).
# It should be:
# t0: unregister
# t1: register
# In this case, it is:
# t0: register # already registered
# t1: unregister
# To avoid this, we should either wait a few frames before tailoring with a new
# profile (wait Godot to clear the previous tailoring), or improve how we handle
# deletion -- possibly with a custom allocator.


func _init():
    pass

func _notification(notification):
    if notification == NOTIFICATION_ENTER_TREE:
        # Register event handlers.
        g_MetaGameEvents.connect("EventBallReleased", g_EventHandlers, "on_event_ball_released_audio")
        g_MetaGameEvents.connect("EventGoal", g_EventHandlers, "on_event_goal_audio")
        g_MetaGameEvents.connect("EventBallHitWall", g_EventHandlers, "on_event_ball_hit_wall_audio")
        g_MetaGameEvents.connect("EventBallHitPaddle", g_EventHandlers, "on_event_ball_hit_paddle_audio")
        # g_MetaGameEvents.connect("EventBallMoved", g_EventHandlers, "on_event_ball_moved_audio")
        # g_MetaGameEvents.connect("EventPaddleMoved", g_EventHandlers, "on_event_paddle_moved_audio")
    elif notification == NOTIFICATION_EXIT_TREE: # NOTIFICATION_PREDELETE:
        # Unregister event handlers.
        g_MetaGameEvents.disconnect("EventBallReleased", g_EventHandlers, "on_event_ball_released_audio")
        g_MetaGameEvents.disconnect("EventGoal", g_EventHandlers, "on_event_goal_audio")
        g_MetaGameEvents.disconnect("EventBallHitWall", g_EventHandlers, "on_event_ball_hit_wall_audio")
        g_MetaGameEvents.disconnect("EventBallHitPaddle", g_EventHandlers, "on_event_ball_hit_paddle_audio")
        # g_MetaGameEvents.disconnect("EventBallMoved", g_EventHandlers, "on_event_ball_moved_audio")
        # g_MetaGameEvents.disconnect("EventPaddleMoved", g_EventHandlers, "on_event_paddle_moved_audio")
