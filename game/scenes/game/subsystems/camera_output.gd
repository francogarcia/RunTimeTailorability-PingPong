extends Camera2D


func _ready():
    set_process(true)

    make_current()

    set_anchor_mode(Camera2D.ANCHOR_MODE_DRAG_CENTER)
    var metaGame = get_node("/root/MetaGame")
    var fieldPosition = metaGame.get_node("Field/Transformable").get_pos()
    var fieldSize = metaGame.get_node("Field/Collidable")
    set_pos(fieldPosition + 0.5 * Vector2(fieldSize.m_Width, fieldSize.m_Height))


func _process(delta):
    # TODO Multiple viewports for multiple views of the game.
    if (Input.is_action_pressed("zoom_in") or Input.is_action_pressed("zoom_out")):
        var camera = self
        var zoom = get_zoom()
        if (Input.is_action_pressed("zoom_out")):
            zoom += Vector2(0.25, 0.25)
        if (Input.is_action_pressed("zoom_in")):
            zoom -= Vector2(0.25, 0.25)
            if (zoom.x <= 0.0):
                zoom.x = 0.25
                zoom.y = 0.25
        set_zoom(zoom)

        var metaGame = get_node("/root/MetaGame")
        var fieldPosition = metaGame.get_node("Field/Transformable").get_pos()
        var fieldSize = metaGame.get_node("Field/Collidable")
        set_pos(fieldPosition + 0.5 * Vector2(fieldSize.m_Width, fieldSize.m_Height))
