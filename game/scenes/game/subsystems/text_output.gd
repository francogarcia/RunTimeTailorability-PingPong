extends Node


func _ready():
    # Register event handlers.
    g_MetaGameEvents.connect("EventBallReleased", self, "on_event_ball_released")
    g_MetaGameEvents.connect("EventGoal", self, "on_event_goal")
    g_MetaGameEvents.connect("EventBallHitWall", self, "on_event_ball_hit_wall")
    g_MetaGameEvents.connect("EventBallHitPaddle", self, "on_event_ball_hit_paddle")
    # g_MetaGameEvents.connect("EventBallMoved", self, "on_event_ball_moved")
    g_MetaGameEvents.connect("EventPaddleMoved", self, "on_event_paddle_moved")


# Event handlers.


func on_event_ball_released():
    get_node("Label").set_text("Ball is rolling!")


func on_event_goal(playerIndex):
    var message = "Player " + str(playerIndex) + " scored!"
    get_node("Label").set_text(message)
    var currentScene = g_SceneManager.get_current_scene()
    message = "Score: Player 1: " + str(currentScene.m_Player1Score) + " - Player 2: " + str(currentScene.m_Player2Score) + "."
    get_node("Label").set_text(message)


func on_event_ball_hit_wall(wallIndex, position):
    var wall
    if (wallIndex == g_SceneManager.get_current_scene().k_WallLeft):
        wall = "left"
    else:
        wall = "right"

    var message = "Ball hit the " + wall + " side of the wall!"
    get_node("Label").set_text(message)


func on_event_ball_hit_paddle(playerIndex, position):
    var message = "Ball hit Player's " + str(playerIndex) + " paddle!"
    get_node("Label").set_text(message)


func on_event_ball_moved(directionVector):
    var horizontal
    if (directionVector.x < 0):
        horizontal = "left"
    elif (directionVector.x > 0):
        horizontal = "right"
    else:
        horizontal = "straight"

    var vertical
    if (directionVector.y < 0):
        vertical = "up"
    elif (directionVector.y > 0):
        vertical = "down"
    else:
        vertical = "straight"

    var message = "Ball is moving " + vertical + " and " + horizontal + "!"
    get_node("Label").set_text(message)


func on_event_paddle_moved(playerIndex, directionIndex):
    var direction
    if (directionIndex == g_SceneManager.get_current_scene().k_PlayerMovementLeft):
        direction = "left"
    else:
        direction = "right"

    var message = "Player " + str(playerIndex) + " is moving " + direction + "."
    get_node("Label").set_text(message)
