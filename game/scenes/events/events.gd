extends Node

# Menu Events

signal EventConfirm
signal EventNextOption(offset)
signal EventSetOption(index)
signal EventOptionChanged(index)

# Game Events

signal EventMovePaddle(playerIndex, directionIndex)

signal EventBallReleased
signal EventGoal(playerIndex)
signal EventBallHitWall(wallIndex, position)
signal EventBallHitPaddle(playerIndex, position)
signal EventBallMoved(directionVector)
signal EventPaddleMoved(playerIndex, directionIndex)
