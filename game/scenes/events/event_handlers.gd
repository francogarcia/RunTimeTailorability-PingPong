extends Node

# TODO It could be useful to move tailor_entity() and untailor_entity() to reuse
# them elsewhere, when needed.

# NOTE As the handlers are called only when the profile is active, checking for
# the existence of the entry could be removed. Keeping for now for safety.

# NOTE Also update untailor_entity() when modifying this.
func tailor_entity(sceneName, entityName, entity):
    var entityComponentTailoring = g_Tailoring.get_interaction_profile()["InteractionProfile"]["Scenes"][sceneName]["ComponentSpecialization"]
    if (entityComponentTailoring.has(entityName)):
        var inputOutputComponents = entityComponentTailoring[entityName]
        for resource in inputOutputComponents:
            g_Tailoring.tailor_suitable_entities(entity,
                                                 g_SceneManager.get_abstract_entity_scene_path(g_SceneManager.kGame, entityName),
                                                 resource)


# NOTE Also update tailor_entity() when modifying this.
func untailor_entity(sceneName, entityName, entity):
    var entityComponentTailoring = g_Tailoring.get_interaction_profile()["InteractionProfile"]["Scenes"][sceneName]["ComponentSpecialization"]
    if (entityComponentTailoring.has(entityName)):
        var inputOutputComponents = entityComponentTailoring[entityName]
        for resource in inputOutputComponents:
            g_Tailoring.untailor_suitable_entities(entity,
                                                   g_SceneManager.get_abstract_entity_scene_path(g_SceneManager.kGame, entityName),
                                                   resource)



# Tailoring-related event handlers.


func on_event_menu_option_changed():
    return


func on_event_menu_option_changed_text_changed_text_description(chosenOption):
    var abstractMenu = g_SceneManager.get_current_scene()
    var chosenOption = abstractMenu.get_selected_option_entity().get_node("ComponentOption").get_description()
    g_SceneManager.get_user_interface_overlay().set_text_description(chosenOption)


# Examples.


# Debug text output.


func on_event_ball_released_debug():
    print("Ball is rolling!")


func on_event_goal_debug(playerIndex):
    var metaGame = get_node("/root/MetaGame")
    print("Player ", playerIndex, " scored!")
    print("Score: Player 1: ", metaGame.m_Player1Score, " - Player 2: ", metaGame.m_Player2Score, ".")

    # var currentScene = g_SceneManager.get_current_scene()
    # print("Score: Player 1 ", currentScene.m_Player1Score, ", Player 2 ", currentScene.m_Player2Score, ".")


func on_event_ball_hit_wall_debug(wallIndex, position):
    var metaGame = get_node("/root/MetaGame")
    var wall
    if (wallIndex == metaGame.k_WallLeft):
        wall = "left"
    else:
        wall = "right"

    print("Ball hit the ", wall, " side of the wall!")


func on_event_ball_hit_paddle_debug(playerIndex, position):
    print("Ball hit Player's ", playerIndex, " paddle!")


func on_event_ball_moved_debug(directionVector):
    var horizontal
    if (directionVector.x < 0):
        horizontal = "left"
    elif (directionVector.x > 0):
        horizontal = "right"
    else:
        horizontal = "straight"

    var vertical
    if (directionVector.y < 0):
        vertical = "up"
    elif (directionVector.y > 0):
        vertical = "down"
    else:
        vertical = "straight"

    print("Ball is moving ", vertical, " and ", horizontal, "!")


func on_event_paddle_moved_debug(playerIndex, directionIndex):
    var metaGame = get_node("/root/MetaGame")
    var direction
    if (directionIndex == metaGame.k_PlayerMovementLeft):
        direction = "left"
    else:
        direction = "right"

    print("Player ", playerIndex, " is moving ", direction, ".")


# Sound effects.


const k_PingPongPaddle = "pingpongbat"
const k_PingPongWall = "pingpongboard"
const k_Goal = "Oooo"
const k_Start = "start_1"
const k_Ball = "ball_1"


func on_event_ball_released_audio():
    g_SceneManager.get_user_interface_overlay().play_sound_effect(k_Start)


func on_event_goal_audio(playerIndex):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(k_Goal)


func on_event_ball_hit_wall_audio(wallIndex, position):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(k_PingPongWall)


func on_event_ball_hit_paddle_audio(playerIndex, position):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(k_PingPongPaddle)


func on_event_ball_moved_audio(directionVector):
    g_SceneManager.get_user_interface_overlay().play_sound_effect(k_Ball)


func on_event_paddle_moved_audio(playerIndex, directionIndex):
    pass
