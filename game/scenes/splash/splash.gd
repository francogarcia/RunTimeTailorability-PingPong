extends Node


func _ready():
    # Optimize the game for less powerful hardware.
    # OS.set_low_processor_usage_mode(true)

    set_fixed_process(true)


func _fixed_process(delta):
    g_SceneManager.change_scene_to(g_SceneManager.k_PingPong)
