extends Node


const k_MaxBallSpeed = 600
const k_PaddleSpeed = Vector2(200, 0)

const k_WallLeft = 0
const k_WallRight = 1

const k_Player1 = 1
const k_Player2 = 2

const k_PlayerMovementLeft = 0
const k_PlayerMovementRight = 1


var m_BallSpeed = null
var m_BallDirection = null

var m_Player1Score = null
var m_Player2Score = null


var m_bPlayer1MoveLeft = null
var m_bPlayer1MoveRight = null
var m_bPlayer2MoveLeft = null
var m_bPlayer2MoveRight = null


func _ready():
    set_process(true)

    randomize()

    # Register game commands.
    g_MetaGameEvents.connect("EventMovePaddle", self, "on_event_move_paddle")

    # Register event handlers.
    # No logic ones.

    # Initialize game data.
    get_node("Field/Collidable").m_Width = 500
    get_node("Field/Collidable").m_Height = 800
    get_node("Ball/Collidable").m_Width = 22
    get_node("Ball/Collidable").m_Height = 22
    get_node("Player1/Collidable").m_Width = 104
    get_node("Player1/Collidable").m_Height = 24
    get_node("Player2/Collidable").m_Width = 104
    get_node("Player2/Collidable").m_Height = 24

    reset_positions()
    reset_kinematics()

    m_Player1Score = 0
    m_Player2Score = 0

    m_bPlayer1MoveLeft = false
    m_bPlayer1MoveRight = false
    m_bPlayer2MoveLeft = false
    m_bPlayer2MoveRight = false


func _process(delta):
    # Process game commands.
    if (m_bPlayer1MoveLeft):
        get_node("Player1/Transformable").translate(delta * -k_PaddleSpeed)
        g_MetaGameEvents.emit_signal("EventPaddleMoved", k_Player1, k_PlayerMovementLeft)
    if (m_bPlayer1MoveRight):
        get_node("Player1/Transformable").translate(delta * k_PaddleSpeed)
        g_MetaGameEvents.emit_signal("EventPaddleMoved", k_Player1, k_PlayerMovementRight)
    if (m_bPlayer2MoveLeft):
        get_node("Player2/Transformable").translate(delta * -k_PaddleSpeed)
        g_MetaGameEvents.emit_signal("EventPaddleMoved", k_Player2, k_PlayerMovementLeft)
    if (m_bPlayer2MoveRight):
        get_node("Player2/Transformable").translate(delta * k_PaddleSpeed)
        g_MetaGameEvents.emit_signal("EventPaddleMoved", k_Player2, k_PlayerMovementRight)
    m_bPlayer1MoveLeft = false
    m_bPlayer1MoveRight = false
    m_bPlayer2MoveLeft = false
    m_bPlayer2MoveRight = false

    # Fetch data from the components.
    var fieldSize = Vector2(get_node("Field/Collidable").m_Width, get_node("Field/Collidable").m_Height)# get_viewport().get_rect().size

    var ballPosition = get_node("Ball/Transformable").get_pos()
    var ballSize = Vector2(get_node("Ball/Collidable").m_Width, get_node("Ball/Collidable").m_Height)

    var player1Position = get_node("Player1/Transformable").get_pos()
    var player1Size = Vector2(get_node("Player1/Collidable").m_Width, get_node("Player1/Collidable").m_Height)

    var player2Position = get_node("Player2/Transformable").get_pos()
    var player2Size = Vector2(get_node("Player2/Collidable").m_Width, get_node("Player2/Collidable").m_Height)

    var ballRectangle = Rect2(ballPosition - 0.5 * ballSize, ballSize)
    var player1Rectangle = Rect2(player1Position - 0.5 * player1Size, player1Size)
    var player2Rectangle = Rect2(player2Position - 0.5 * player2Size, player2Size)

    # Rule: move ball whenever it has non-zero velocity.
    ballPosition += m_BallDirection * m_BallSpeed * delta
    # Rule: detect ball collision with walls.
    if (ballPosition.x > fieldSize.x):
        ballPosition.x = fieldSize.x
        m_BallDirection.x *= -1
        g_MetaGameEvents.emit_signal("EventBallHitWall", k_WallRight, ballPosition)
    elif (ballPosition.x < 0):
        ballPosition.x = 0
        m_BallDirection.x *= -1
        g_MetaGameEvents.emit_signal("EventBallHitWall", k_WallLeft, ballPosition)

    var bBallHitPlayer1 = player1Rectangle.has_point(ballPosition)
    var bBallHitPlayer2 = player2Rectangle.has_point(ballPosition)
    # Rule: detect ball collision with paddles (rebound).
    if ((bBallHitPlayer1) or (bBallHitPlayer2)):
        m_BallDirection.y *= -1
        m_BallSpeed *= 1.1
        if (m_BallSpeed > k_MaxBallSpeed):
            m_BallSpeed = k_MaxBallSpeed
        if (bBallHitPlayer1):
            g_MetaGameEvents.emit_signal("EventBallHitPaddle", k_Player1, ballPosition)
        else:
            g_MetaGameEvents.emit_signal("EventBallHitPaddle", k_Player2, ballPosition)
    else:
        get_node("Ball/Transformable").set_pos(ballPosition)
        g_MetaGameEvents.emit_signal("EventBallMoved", m_BallDirection)

    # Rule: detect ball collision with walls (goal).
    if (ballPosition.y > fieldSize.y):
        reset_positions()
        reset_kinematics()
        m_Player1Score += 1
        g_MetaGameEvents.emit_signal("EventGoal", k_Player1)
    elif (ballPosition.y < 0):
        reset_positions()
        reset_kinematics()
        m_Player2Score += 1
        g_MetaGameEvents.emit_signal("EventGoal", k_Player2)


func reset_positions():
    # var fieldSize = get_viewport().get_rect().size
    # get_node("Field/Collidable").m_Width = fieldSize.x
    # get_node("Field/Collidable").m_Height = fieldSize.y

    var fieldSize = Vector2(get_node("Field/Collidable").m_Width, get_node("Field/Collidable").m_Height)
    var fieldCenter = 0.5 * fieldSize
    var playerSize = Vector2(get_node("Player1/Collidable").m_Width, get_node("Player1/Collidable").m_Height)
    var halfPlayerSize = 0.5 * playerSize
    var ballSize = Vector2(get_node("Ball/Collidable").m_Width, get_node("Ball/Collidable").m_Height)
    get_node("Player1/Transformable").set_pos(Vector2(fieldCenter.x, playerSize.y))
    get_node("Player2/Transformable").set_pos(Vector2(fieldCenter.x, fieldSize.y - playerSize.y))
    get_node("Ball/Transformable").set_pos(Vector2(fieldCenter.x, fieldCenter.y - 0.5 * ballSize.y))


func reset_kinematics():
    m_BallDirection = Vector2(one_or_negative_one(), one_or_negative_one())
    m_BallSpeed = 0.25 * k_MaxBallSpeed

    g_MetaGameEvents.emit_signal("EventBallReleased")


func one_or_negative_one():
    if (randf() < 0.5):
        return -1
    return 1


# Event handlers for game commands.


func on_event_move_paddle(playerIndex, directionIndex):
    if (playerIndex == k_Player1):
        if (directionIndex == k_PlayerMovementLeft):
            m_bPlayer1MoveLeft = true
        else:
            m_bPlayer1MoveRight = true
    elif (playerIndex == k_Player2):
        if (directionIndex == k_PlayerMovementLeft):
            m_bPlayer2MoveLeft = true
        else:
            m_bPlayer2MoveRight = true


# Event handlers for logic events.
