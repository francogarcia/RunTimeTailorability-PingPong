extends Node


func _ready():
    set_process(true)


func _process(delta):
    ai_input()


func ai_input():
    var metaGame = get_node("/root/MetaGame")
    var ballPosition = metaGame.get_node("Ball/Transformable").get_pos()

    if (metaGame.get_node("Player1").has_node("AIInput")):
        var player1Size = Vector2(metaGame.get_node("Player1/Collidable").m_Width, metaGame.get_node("Player1/Collidable").m_Height)
        var player1Position = metaGame.get_node("Player1/Transformable").get_pos()

        var player1MoveDirection = Vector2(0.0, 0.0)
        if (metaGame.m_BallDirection.y < 0.0):
            player1MoveDirection = player1Position - ballPosition
        if (player1MoveDirection.x > 0.0):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player1, metaGame.k_PlayerMovementLeft)
        elif (player1MoveDirection.x < 0.0):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player1, metaGame.k_PlayerMovementRight)

    if (metaGame.get_node("Player2").has_node("AIInput")):
        var player2Size = Vector2(metaGame.get_node("Player2/Collidable").m_Width, metaGame.get_node("Player2/Collidable").m_Height)
        var player2Position = metaGame.get_node("Player2/Transformable").get_pos()

        var player2MoveDirection = Vector2(0.0, 0.0)
        if (metaGame.m_BallDirection.y > 0.0):
            player2MoveDirection = player2Position - ballPosition
        if (player2MoveDirection.x > 0.0):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player2, metaGame.k_PlayerMovementLeft)
        elif (player2MoveDirection.x < 0.0):
            g_MetaGameEvents.emit_signal("EventMovePaddle", metaGame.k_Player2, metaGame.k_PlayerMovementRight)
