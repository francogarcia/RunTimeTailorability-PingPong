extends CanvasLayer


# NOTE To change the quantity of simultaneous sounds, change "Polyphony"
# property.

# NOTE The original sounds were converted to WAV using the following script:
# #!/bin/bash
# INPUT_FORMAT=ogg
# OUTPUT_FORMAT=wav
# PATH=game/assets/aj_/
# pushd ${PATH}
# for file in *.${INPUT_FORMAT}; do
#     extension="${file##*.}"
#     fileName="${file%.*}"
#     ffmpeg -i ${file} ${fileName}.${OUTPUT_FORMAT}
# done
# popd


var m_SamplePlayer = null
var m_StreamPlayer = null

var m_CurrentStream = null

# Music resources.
# var m_MainMusic = null

# Sound effects
var m_SoundEffects = null
var m_Instructions = null

# Interaction Profile
var m_CustomInteractionProfilePath = null


func _ready():
    set_fixed_process(true)

    m_SamplePlayer = get_node("SamplePlayer2D")
    m_StreamPlayer = get_node("StreamPlayer")

    # TODO Set default values for volume. Also set the value in play_sound_effect().
    # m_SamplePlayer.set_default_volume(1.0)
    # m_StreamPlayer.set_volume(1.0)

    # Load music resources.
    # m_MainMusic = load("res://file.ogg")

    # Currently playing sounds
    m_SoundEffects = []
    m_Instructions = []

    # Interaction profile settings.
    register_interaction_profiles()
    m_CustomInteractionProfilePath = null
    set_fixed_process(true)


# Destructor.
func _notification(notification):
    if notification == NOTIFICATION_PREDELETE:
       unregister_interaction_profiles()


func _fixed_process(delta):
    if (Input.is_action_just_pressed("tailor")):
        g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring...")
        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())
    if (Input.is_action_just_pressed("untailor")):
        g_SceneManager.get_user_interface_overlay().set_text_description("Untailoring...")
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())
    if ((Input.is_action_just_pressed("load_profile_usual")) or
        (Input.is_action_just_pressed("load_profile_usual_first_version")) or
        (Input.is_action_just_pressed("load_profile_usual_text_description")) or
        (Input.is_action_just_pressed("load_profile_usual_graphical_effects")) or
        (Input.is_action_just_pressed("load_profile_usual_sound_effects")) or
        (Input.is_action_just_pressed("load_profile_usual_automated_input")) or
        (Input.is_action_just_pressed("load_profile_usual_automated_input_partial")) or
        (Input.is_action_just_pressed("load_profile_usual_zoom")) or
        (Input.is_action_just_pressed("load_profile_meta_game_debug_drawing"))):
        g_Tailoring.untailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())

        if (Input.is_action_just_pressed("load_profile_usual")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game.json")
        elif (Input.is_action_just_pressed("load_profile_usual_first_version")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the first version of the game (graphics only, two human players)...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_first_version.json")
        elif (Input.is_action_just_pressed("load_profile_usual_text_description")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with text descriptions...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_text_description.json")
        elif (Input.is_action_just_pressed("load_profile_usual_graphical_effects")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with graphical effects...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_graphical_effects.json")
        elif (Input.is_action_just_pressed("load_profile_usual_sound_effects")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with sound effects...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_sound_effects_subsystem.json")
            # g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_sound_effects_event_handlers.json")
        elif (Input.is_action_just_pressed("load_profile_usual_automated_input")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with input automation...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_automated_input.json")
        elif (Input.is_action_just_pressed("load_profile_usual_automated_input_partial")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with partial input automation...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_hybrid_input.json")
        elif (Input.is_action_just_pressed("load_profile_usual_zoom")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring to the usual game profile with zoom...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/usual_game_zoom.json")
        elif (Input.is_action_just_pressed("load_profile_meta_game_debug_drawing")):
            g_SceneManager.get_user_interface_overlay().set_text_description("Tailoring with debug drawing...")
            g_Tailoring.load_and_set_interaction_profile("res://profiles/meta_game_debug_drawing.json")
        else:
            asssert(!"Invalid profile!")

        g_Tailoring.tailor_to_profile(g_Tailoring.get_interaction_profile(), g_SceneManager.get_current_scene_name())


func set_text_description(description):
    get_node("Label").set_text(description)
    get_node("Label").show()
    get_node("Timer").start()


func get_custom_interaction_profile_path():
    return m_CustomInteractionProfilePath


func set_custom_interaction_profile_path(interactionProfileFilename):
    assert(interactionProfileFilename != null)

    m_CustomInteractionProfilePath = interactionProfileFilename


# NOTE This is for testing tailoring/untailoring during the game.
func register_interaction_profiles():
    InputMap.add_action("tailor")
    InputMap.add_action("untailor")
    InputMap.add_action("load_profile_usual")
    InputMap.add_action("load_profile_usual_first_version")
    InputMap.add_action("load_profile_usual_text_description")
    InputMap.add_action("load_profile_usual_graphical_effects")
    InputMap.add_action("load_profile_usual_sound_effects")
    InputMap.add_action("load_profile_usual_automated_input")
    InputMap.add_action("load_profile_usual_automated_input_partial")
    InputMap.add_action("load_profile_usual_zoom")
    InputMap.add_action("load_profile_meta_game_debug_drawing")

    g_Tailoring.register_keyboard_key("tailor", "f1")
    g_Tailoring.register_keyboard_key("untailor", "f2")
    g_Tailoring.register_keyboard_key("load_profile_usual", "f3")
    g_Tailoring.register_keyboard_key("load_profile_usual_first_version", "f4")
    g_Tailoring.register_keyboard_key("load_profile_usual_text_description", "f5")
    g_Tailoring.register_keyboard_key("load_profile_usual_graphical_effects", "f6")
    g_Tailoring.register_keyboard_key("load_profile_usual_sound_effects", "f7")
    g_Tailoring.register_keyboard_key("load_profile_usual_automated_input", "f9")
    g_Tailoring.register_keyboard_key("load_profile_usual_automated_input_partial", "f10")
    g_Tailoring.register_keyboard_key("load_profile_usual_zoom", "f11")
    g_Tailoring.register_keyboard_key("load_profile_meta_game_debug_drawing", "f12")


func unregister_interaction_profiles():
    InputMap.erase_action("tailor")
    InputMap.erase_action("untailor")
    InputMap.erase_action("load_profile_usual")
    InputMap.erase_action("load_profile_usual_first_version")
    InputMap.erase_action("load_profile_usual_text_description")
    InputMap.erase_action("load_profile_usual_graphical_effects")
    InputMap.erase_action("load_profile_usual_sound_effects")
    InputMap.erase_action("load_profile_usual_automated_input")
    InputMap.erase_action("load_profile_usual_automated_input_partial")
    InputMap.erase_action("load_profile_usual_zoom")
    InputMap.erase_action("load_profile_meta_game_debug_drawing")


# Play a sound effect, and return its ID (voiceID).
func play_sound_effect(soundKey):
    var voiceID = m_SamplePlayer.play(soundKey)
    # m_SamplePlayer.set_volume(voiceID, 1.0)
    m_SoundEffects.push_back(voiceID)
    return voiceID


func stop_sound_effect(voiceID):
    m_SamplePlayer.stop(voiceID)


func stop_music():
    if (m_StreamPlayer.is_playing()):
        m_StreamPlayer.stop()


func _play_music():
    stop_music()

    m_StreamPlayer.set_stream(m_CurrentStream)
    m_StreamPlayer.set_loop(true)

    m_StreamPlayer.play()


# func play_main_music():
#     m_CurrentStream = m_MainMusic
#     _play_music()


func _on_Timer_timeout():
    get_node("Label").hide()
